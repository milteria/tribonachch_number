GO111MODULE := on
MODULE_NAME:=$(shell sh -c 'cat go.mod | grep module | sed -e "s/module //"')
DOCKER_IMAGE_NAME=tribunachch_number
DOCKER_IMAGE_NAME_BUILDING="${DOCKER_IMAGE_NAME}/building"
DOCKERFILE_PATH=${PWD}/build/Dockerfile
DOCKER_CONTAINER_NAME=tribonachch
HTTP_ADDR="127.0.0.1:10000"

all: prepare format
prepare:
	go mod download
	go generate ./...
.PHONY: build
build:
	docker build -t ${DOCKER_IMAGE_NAME} -f ${DOCKERFILE_PATH} .
build_source:
	docker build -t ${DOCKER_IMAGE_NAME_BUILDING} -f ${DOCKERFILE_PATH} --target building .
start: clear
	docker run --rm -d --name ${DOCKER_CONTAINER_NAME} -p ${HTTP_ADDR}:10000  ${DOCKER_IMAGE_NAME}
stop:
	docker stop ${DOCKER_CONTAINER_NAME}

clear:
	docker rm --force ${DOCKER_CONTAINER_NAME} || true
	rm -f coverage.out
	rm -f coverage.html

tests:
	go test -v ./...
coverage:
	go test -covermode=count -coverprofile=coverage.out `go list ./...` | grep -q ""
	go tool cover -html=coverage.out -o coverage.html
	go tool cover -func=coverage.out

format:
	go fmt `go list ./... | grep -v /vendor/`
	goimports -w -local ${MODULE_NAME} `go list -f {{.Dir}} ./...`
linter: format
	gometalinter ./...

sequence_benchmark:
	go test -bench=. -benchmem ./sequence

docker_tests: build_source
	docker run --rm ${DOCKER_IMAGE_NAME_BUILDING} make tests
docker_coverage: build_source
	docker run --rm ${DOCKER_IMAGE_NAME_BUILDING} make coverage
docker_linter: build_source
	docker run --rm -v ${PWD}:/src/tribonachch_number ${DOCKER_IMAGE_NAME_BUILDING} make linter
docker_sequence_benchmark:
	docker run --rm ${DOCKER_IMAGE_NAME_BUILDING} make sequence_benchmark