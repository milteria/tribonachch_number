package main

import (
	"context"
	"fmt"
	defaultlog "log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	gorillahanders "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"tribonachch_number/handlers"
	"tribonachch_number/sequence"
)

func main() {
	log, err := zap.NewDevelopment()
	if err != nil {
		defaultlog.Fatal(err)
	}

	defaultLog, err := zap.NewStdLogAt(log, zapcore.InfoLevel)
	if err != nil {
		log.Fatal(err.Error())
	}

	httpLog := log.With(zap.String("module", "http_server"))

	router := mux.NewRouter()
	router.NotFoundHandler = handlers.NewNotFoundHandler(httpLog)
	router.MethodNotAllowedHandler = handlers.NewMethodNotAllowedHandler(httpLog)

	router.Handle("/sequence/elements/{n:[0-9]+}",
		handlers.NewSequenceElementHandler(httpLog, sequence.NewLinearImplementation()),
	).Methods(http.MethodGet)

	httpAddr, ok := os.LookupEnv("HTTP_ADDR")
	if !ok {
		httpAddr = ":10000"
	}

	server := http.Server{
		Addr: httpAddr,
		Handler: gorillahanders.LoggingHandler(defaultLog.Writer(),
			gorillahanders.CORS()(router)),
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			log.Fatal(err.Error())
		}
	}()
	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		err := server.Shutdown(ctx)
		if err != nil {
			log.Error(err.Error())
		}
		log.Info("server is stopped")
	}()
	log.Info(fmt.Sprintf("server is started on '%s'", server.Addr))

	sig := make(chan os.Signal, 1)
	defer close(sig)

	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	<-sig
}
