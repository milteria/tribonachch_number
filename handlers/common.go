package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"reflect"
	"unsafe"

	"go.uber.org/zap"
)

var internalServerErrorResponse []byte

func init() {
	var err error
	resp := &httpError{
		code: 500,
		msg:  "internal error",
	}
	internalServerErrorResponse, err = json.Marshal(resp)
	if err != nil {
		log.Fatal(err)
	}
}

func internalServerErrorHandler(log *zap.Logger, w http.ResponseWriter) {
	writeResponse(log, w, http.StatusInternalServerError, internalServerErrorResponse)
}

func writeResponse(log *zap.Logger, w http.ResponseWriter, code int, data []byte) {
	w.WriteHeader(code)

	_, err := w.Write(data)
	if err != nil {
		log.Error(err.Error())
	}
}

func sendResponse(log *zap.Logger, w http.ResponseWriter, code int, response json.Marshaler) {
	data, err := json.Marshal(response)
	if err != nil {
		log.Error(err.Error())
		internalServerErrorHandler(log, w)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	writeResponse(log, w, code, data)
}

const (
	successCreationHandler = "handler is created"

	module                      = "handler"
	notFoundHandlerName         = "not_found"
	methodNotAllowedHandlerName = "method_no_allowed"
	sequenceHandlerName         = "sequence_element"
)

// s2b converts string to a byte slice without memory allocation.
func s2b(s string) []byte {
	sh := (*reflect.StringHeader)(unsafe.Pointer(&s))
	bh := reflect.SliceHeader{
		Data: sh.Data,
		Len:  sh.Len,
		Cap:  sh.Len,
	}
	return *(*[]byte)(unsafe.Pointer(&bh))
}
