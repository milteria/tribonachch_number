package handlers

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

//go:generate mockgen -destination json_mock_test.go -package handlers encoding/json Marshaler
//go:generate mockgen -destination http_response_writer_mock_test.go -package handlers net/http ResponseWriter

func testMarshalResponse(t *testing.T, r *http.Request, handler http.Handler,
	response *MockMarshaler) {
	t.Run("marshal return error", func(t *testing.T) {
		w := httptest.NewRecorder()
		defer w.Flush()
		response.EXPECT().MarshalJSON().Return(nil, errors.New("test"))

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.NotNil(t, w.Body)
		assert.True(t, w.Body.Len() > 0)
	})

	t.Run("marshal is correct", func(t *testing.T) {
		w := httptest.NewRecorder()
		defer w.Flush()
		wantBody := []byte("{}")
		response.EXPECT().MarshalJSON().Return(wantBody, nil)

		handler.ServeHTTP(w, r)

		assert.NotNil(t, w.Body)
		assert.EqualValues(t, wantBody, w.Body.Bytes())
	})
}

func testResponseWriter(t *testing.T, r *http.Request, handler http.Handler, response *MockMarshaler) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	w := NewMockResponseWriter(ctrl)
	w.EXPECT().Header().Return(http.Header{}).AnyTimes()

	t.Run("response writer return error", func(t *testing.T) {
		w.EXPECT().Write(gomock.Any()).Return(0, errors.New("text"))
		w.EXPECT().WriteHeader(gomock.Any())
		response.EXPECT().MarshalJSON().Return([]byte("{}"), nil)

		assert.NotPanics(t, func() {
			handler.ServeHTTP(w, r)
		})
	})

	t.Run("response writer is correct", func(t *testing.T) {
		w.EXPECT().Write(gomock.Any()).Return(1, nil)
		w.EXPECT().WriteHeader(gomock.Any())
		response.EXPECT().MarshalJSON().Return([]byte("{}"), nil)

		assert.NotPanics(t, func() {
			handler.ServeHTTP(w, r)
		})
	})
}
