package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"go.uber.org/zap"
)

type httpError struct {
	code int
	msg  string
}

func (e *httpError) MarshalJSON() ([]byte, error) {
	return s2b(fmt.Sprintf(`{
	"code":%d,
	"message":"%s"
}`, e.code, e.msg)), nil
}

// NewNotFoundHandler constructs a new 'not found' http handler
func NewNotFoundHandler(log *zap.Logger) http.Handler {
	handler := &notFoundHandler{
		response: &httpError{
			code: 404,
			msg:  "not found",
		},
		log: log.With(zap.String(module, notFoundHandlerName)),
	}
	defer handler.log.Debug(successCreationHandler)

	return handler
}

type notFoundHandler struct {
	response json.Marshaler
	log      *zap.Logger
}

func (h *notFoundHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sendResponse(h.log, w, http.StatusNotFound, h.response)
}

// NewMethodNotAllowedHandler constructs a new 'method not allowed' http handler
func NewMethodNotAllowedHandler(log *zap.Logger) http.Handler {
	handler := &methodNotAllowedHandler{
		response: &httpError{
			code: 405,
			msg:  "method not allowed",
		},
		log: log.With(zap.String(module, methodNotAllowedHandlerName)),
	}
	defer handler.log.Debug(successCreationHandler)

	return handler
}

type methodNotAllowedHandler struct {
	response json.Marshaler
	log      *zap.Logger
}

func (h *methodNotAllowedHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	sendResponse(h.log, w, http.StatusMethodNotAllowed, h.response)
}

func badRequest(log *zap.Logger, w http.ResponseWriter, msg string) {
	resp := &httpError{
		code: http.StatusBadRequest,
		msg:  msg,
	}

	sendResponse(log, w, http.StatusBadRequest, resp)
}
