package handlers

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
)

func Test_httpError_MarshalJSON(t *testing.T) {
	model := &httpError{
		code: 123,
		msg:  "test",
	}

	data, err := model.MarshalJSON()

	assert.NoError(t, err)
	assert.EqualValues(t, s2b(`{
	"code":123,
	"message":"test"
}`), data)
	assert.Equal(t, 123, model.code)
	assert.Equal(t, "test", model.msg)
}

func TestNewNotFoundHandler(t *testing.T) {
	log := zaptest.NewLogger(t)

	handler := NewNotFoundHandler(log)

	assert.NotNil(t, handler)
	assert.Implements(t, (*http.Handler)(nil), handler)
	assert.IsType(t, &notFoundHandler{}, handler)

	convertedHandler := handler.(*notFoundHandler)
	assert.NotNil(t, convertedHandler.log)
	assert.NotNil(t, convertedHandler.response)

	dataResp, err := convertedHandler.response.MarshalJSON()
	assert.NoError(t, err)
	assert.EqualValues(t, s2b(`{
	"code":404,
	"message":"not found"
}`), dataResp)
}

func Test_notFoundHandler_ServeHTTP(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	log := zaptest.NewLogger(t)

	t.Run("common tests", func(t *testing.T) {
		resp := NewMockMarshaler(ctrl)
		handler := &notFoundHandler{
			log:      log,
			response: resp,
		}

		t.Run("test send response", func(t *testing.T) {
			r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))

			testMarshalResponse(t, r, handler, resp)
		})

		t.Run("test response writer", func(t *testing.T) {
			r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))

			testResponseWriter(t, r, handler, resp)
		})
	})

	t.Run("correct response", func(t *testing.T) {
		handler := NewNotFoundHandler(log)

		r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))
		w := httptest.NewRecorder()
		defer w.Flush()

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusNotFound, w.Code)
		assert.NotNil(t, w.Body)
		assert.EqualValues(t, `{"code":404,"message":"not found"}`, w.Body.String())
	})
}

func TestNewMethodNotAllowedHandler(t *testing.T) {
	log := zaptest.NewLogger(t)

	handler := NewMethodNotAllowedHandler(log)

	assert.NotNil(t, handler)
	assert.Implements(t, (*http.Handler)(nil), handler)
	assert.IsType(t, &methodNotAllowedHandler{}, handler)

	convertedHandler := handler.(*methodNotAllowedHandler)
	assert.NotNil(t, convertedHandler.log)
	assert.NotNil(t, convertedHandler.response)

	dataResp, err := convertedHandler.response.MarshalJSON()
	assert.NoError(t, err)
	assert.EqualValues(t, s2b(`{
	"code":405,
	"message":"method not allowed"
}`), dataResp)
}

func Test_methodNotAllowedHandler_ServeHTTP(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	log := zaptest.NewLogger(t)

	t.Run("common tests", func(t *testing.T) {
		resp := NewMockMarshaler(ctrl)
		handler := &methodNotAllowedHandler{
			log:      log,
			response: resp,
		}

		t.Run("test send response", func(t *testing.T) {
			r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))

			testMarshalResponse(t, r, handler, resp)
		})

		t.Run("test response writer", func(t *testing.T) {
			r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))

			testResponseWriter(t, r, handler, resp)
		})
	})

	t.Run("correct response", func(t *testing.T) {
		handler := NewMethodNotAllowedHandler(log)

		r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))
		w := httptest.NewRecorder()
		defer w.Flush()

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusMethodNotAllowed, w.Code)
		assert.NotNil(t, w.Body)
		assert.EqualValues(t, `{"code":405,"message":"method not allowed"}`, w.Body.String())
	})
}
