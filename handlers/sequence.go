package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"tribonachch_number/sequence"
)

type sequenceElementHandler struct {
	log *zap.Logger

	sequence sequence.Sequence
}

// NewSequenceElementHandler constructs a new http handler for getting sequence element by number
// ('n' parameter in query)
func NewSequenceElementHandler(log *zap.Logger, sequence sequence.Sequence) http.Handler {
	handler := &sequenceElementHandler{
		log:      log.With(zap.String(module, sequenceHandlerName)),
		sequence: sequence,
	}
	defer handler.log.Debug(successCreationHandler)

	return handler
}

type sequenceElement struct {
	value uint64
}

func (m *sequenceElement) MarshalJSON() ([]byte, error) {
	return s2b(fmt.Sprintf(`{"element": %d}`, m.value)), nil
}

var maxN = fmt.Sprint(uint64(1<<64 - 1))

func (h *sequenceElementHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	nParam, ok := mux.Vars(r)["n"]
	if !ok {
		h.log.Error("not set parameter 'n'")
		internalServerErrorHandler(h.log, w)
		return
	}

	n, err := strconv.ParseUint(nParam, 10, 64)
	if err != nil {
		switch input := err.(type) {
		case *strconv.NumError:
			if input.Err == strconv.ErrRange {
				badRequest(h.log, w, fmt.Sprintf("the sequence element must be in interval from 0 to %s", maxN))
				return
			}
		}
		h.log.Error(err.Error())
		internalServerErrorHandler(h.log, w)
		return
	}

	resp := &sequenceElement{
		value: h.sequence.Get(n),
	}
	sendResponse(h.log, w, 200, resp)
}
