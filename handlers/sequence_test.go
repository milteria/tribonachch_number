package handlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zaptest"
)

//go:generate mockgen -destination sequence_mock_test.go -package handlers tribonachch_number/sequence Sequence

func TestNewSequenceElementHandler(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	log := zaptest.NewLogger(t)
	seq := NewMockSequence(ctrl)

	handler := NewSequenceElementHandler(log, seq)

	assert.NotNil(t, handler)
	assert.Implements(t, (*http.Handler)(nil), handler)
	assert.IsType(t, &sequenceElementHandler{}, handler)

	convertedHandler := handler.(*sequenceElementHandler)
	assert.NotNil(t, convertedHandler.log)
	assert.NotNil(t, convertedHandler.sequence)
	assert.True(t, convertedHandler.sequence == seq)
}

func Test_sequenceElement_MarshalJSON(t *testing.T) {
	model := &sequenceElement{
		value: 123,
	}

	data, err := model.MarshalJSON()

	assert.NoError(t, err)
	assert.EqualValues(t, s2b(`{"element": 123}`), data)
	assert.Equal(t, uint64(123), model.value)
}

func Test_sequenceElementHandler_ServeHTTP(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	log := zaptest.NewLogger(t)
	seq := NewMockSequence(ctrl)
	handler := NewSequenceElementHandler(log, seq)

	t.Run("without 'n' parameter", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))
		w := httptest.NewRecorder()
		defer w.Flush()

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.NotNil(t, w.Body)
		assert.True(t, w.Body.Len() > 0)
	})

	t.Run("incorrect 'n' parameter", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))
		r = mux.SetURLVars(r, map[string]string{
			"n": "test",
		})
		w := httptest.NewRecorder()
		defer w.Flush()

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusInternalServerError, w.Code)
		assert.NotNil(t, w.Body)
		assert.True(t, w.Body.Len() > 0)
	})

	t.Run("'n' out of range", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))
		r = mux.SetURLVars(r, map[string]string{
			"n": "1111111111111111111111111111111111111111111111111111111111111111111",
		})
		w := httptest.NewRecorder()
		defer w.Flush()

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusBadRequest, w.Code)
		assert.NotNil(t, w.Body)
		assert.True(t, w.Body.Len() > 0)
	})

	t.Run("correct 'n' parameter", func(t *testing.T) {
		r := httptest.NewRequest(http.MethodGet, "/test", strings.NewReader(""))
		r = mux.SetURLVars(r, map[string]string{
			"n": "123",
		})
		w := httptest.NewRecorder()
		defer w.Flush()
		seqElement := uint64(123456789)
		seq.EXPECT().Get(gomock.Any()).Return(seqElement)

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusOK, w.Code)
		assert.NotNil(t, w.Body)
		assert.EqualValues(t, fmt.Sprintf(`{"element":%d}`, seqElement), w.Body.String())
	})
}
