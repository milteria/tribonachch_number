package sequence

// NewLinearImplementation constructs implementation with linear complexity of an algorithm
func NewLinearImplementation() Sequence {
	return &linearImplementation{}
}

type linearImplementation struct{}

func (*linearImplementation) Get(n uint64) uint64 {
	seq := map[uint64]uint64{
		0: 0,
		1: 0,
		2: 1,
	}

	for i := uint64(3); i <= n; i++ {
		seq[i] = seq[i-1] + seq[i-2] + seq[i-3]
		delete(seq, i-3)
	}

	return seq[n]
}
