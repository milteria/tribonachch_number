package sequence

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewLinearImplementation(t *testing.T) {
	seq := NewLinearImplementation()

	assert.NotNil(t, seq)
	assert.Implements(t, (*Sequence)(nil), seq)
	assert.IsType(t, &linearImplementation{}, seq)
}

func Test_linearImplementation_Get(t *testing.T) {
	seq := NewLinearImplementation()

	testGettingElement(t, seq)
}
