package sequence

// NewMatrixImplementation constructs implementation with matrix algorithm
func NewMatrixImplementation() Sequence {
	return &matrixImplementation{
		q: [3][3]uint64{
			{0, 0, 1},
			{1, 0, 1},
			{0, 1, 1},
		},
	}
}

type matrixImplementation struct {
	q [3][3]uint64
}

func (seq *matrixImplementation) Get(n uint64) uint64 {
	if n == 0 {
		return seq.powMatrix(seq.q, n)[0][1]
	}

	oddPow := n
	var countSqrt uint64
	for (oddPow % 2) == 0 {
		countSqrt++
		oddPow = oddPow >> 1
	}

	matrix := seq.powMatrix(seq.q, oddPow)

	for i := uint64(0); i < countSqrt; i++ {
		matrix = seq.powMatrix(matrix, 2)
	}

	return matrix[0][1]
}

func (seq *matrixImplementation) powMatrix(q [3][3]uint64, n uint64) (result [3][3]uint64) {
	switch n {
	case 0:
		// single matrix
		return [3][3]uint64{
			{1, 0, 0},
			{0, 1, 0},
			{0, 0, 1},
		}
	case 1:
		return q
	}

	var currentMatrix = q

	for i := uint64(1); i < n; i++ {
		for row := 0; row < len(currentMatrix); row++ {
			for column := 0; column < len(currentMatrix[row]); column++ {
				var mulColumn [3]uint64
				for j := 0; j < 3; j++ {
					mulColumn[j] = q[j][column]
				}
				result[row][column] = seq.mulColumnRow(mulColumn, currentMatrix[row])
			}
		}
		currentMatrix = result
	}

	return
}

func (*matrixImplementation) mulColumnRow(column, row [3]uint64) uint64 {
	var sum uint64
	for i := 0; i < 3; i++ {
		sum += column[i] * row[i]
	}

	return sum
}
