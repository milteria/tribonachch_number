package sequence

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewMatrixImplementation(t *testing.T) {
	seq := NewMatrixImplementation()

	assert.NotNil(t, seq)
	assert.Implements(t, (*Sequence)(nil), seq)
	assert.IsType(t, &matrixImplementation{}, seq)

	matrixImpl := seq.(*matrixImplementation)
	assert.Len(t, matrixImpl.q, 3)

	for i := 0; i < 3; i++ {
		assert.Len(t, matrixImpl.q[i], 3)
	}
}

func Test_matrixImplementation_Get(t *testing.T) {
	seq := NewMatrixImplementation()

	testGettingElement(t, seq)
}
