package sequence

// NewRecursiveImplementation constructs implementation with exponential complexity of an algorithm
func NewRecursiveImplementation() Sequence {
	return &recursiveImplementation{}
}

type recursiveImplementation struct{}

func (seq *recursiveImplementation) Get(n uint64) uint64 {
	switch n {
	case 0, 1:
		return 0
	case 2:
		return 1
	}

	var sum uint64

	for i := uint64(1); i <= 3; i++ {
		sum += seq.Get(n - i)
	}

	return sum
}
