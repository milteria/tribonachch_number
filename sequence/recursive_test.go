package sequence

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRecursiveImplementation(t *testing.T) {
	seq := NewRecursiveImplementation()

	assert.NotNil(t, seq)
	assert.Implements(t, (*Sequence)(nil), seq)
	assert.IsType(t, &recursiveImplementation{}, seq)
}

func Test_recursiveImplementation_Get(t *testing.T) {
	seq := NewRecursiveImplementation()

	testGettingElement(t, seq)
}
