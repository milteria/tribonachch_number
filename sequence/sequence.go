package sequence

// Sequence of tribonachch numbers
type Sequence interface {
	Get(n uint64) uint64
}
