package sequence

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func testGettingElement(t *testing.T, sequence Sequence) {
	wantResult := []uint64{0, 0, 1, 1, 2, 4, 7, 13, 24, 44, 81, 149, 274, 504, 927, 1705, 3136, 5768, 10609, 19513,
		35890}

	for i := uint64(0); i < uint64(len(wantResult)); i++ {
		t.Run(fmt.Sprintf("getting %d element", i), func(t *testing.T) {
			value := sequence.Get(i)

			assert.Equal(t, wantResult[i], value)
		})
	}
}

func BenchmarkLinearImplementation_Get(b *testing.B) {
	b.ReportAllocs()
	linearSeq := NewLinearImplementation()

	for i := 0; i < b.N; i++ {
		n := uint64(i)
		linearSeq.Get(n)
	}
}

func BenchmarkMatrixImplementation_Get(b *testing.B) {
	b.ReportAllocs()
	linearSeq := NewMatrixImplementation()

	for i := 0; i < b.N; i++ {
		n := uint64(i)
		linearSeq.Get(n)
	}
}

func BenchmarkRecursiveImplementation_Get(b *testing.B) {
	b.ReportAllocs()
	recursiveSeq := NewRecursiveImplementation()

	for i := 0; i < 30; i++ {
		n := uint64(i)
		recursiveSeq.Get(n)
	}
}
